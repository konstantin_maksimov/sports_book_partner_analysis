#standardSQL

select    final.ClientId,
          final.BetId,
          final.BetType,
          final.BetState,
          final.PartnerId,
          final.BetCreated,
          final.BetAmountInEURC,
          final.BetWinningAmountInEURC,
          final.BetCurrencyId,
          final.ClientBirthRegionId,
          final.BetSource,
          final.BetIsLive,
          final.SportId from (
                                select
                                      ROW_NUMBER() OVER(PARTITION BY bs.BetId order by count(bs.SportId) desc) as row_number,
                                      bs.ClientId,
                                      bs.BetId,
                                      bs.BetType,
                                      bs.BetState,
                                      bs.PartnerId,
                                      bs.BetCreated,
                                      bs.BetAmountInEURC,
                                      bs.BetWinningAmountInEURC,
                                      bs.BetCurrencyId,
                                      bs.ClientBirthRegionId,
                                      bs.BetSource,
                                      bs.BetIsLive,
                                      bs.SportId
                                from
                                      `antifraud-testing.dwh_kafka_sink_1.connect_dwh_sportsbook_betselection_v3` as bs
                                where
                                      bs.BetCreated >= '{}' and
                                      bs.BetCreated <= '{}' and
                                      bs.PartnerId = {} and
                                      bs.BetCalcDate is not null and
                                      bs.BetModified = (select max(tmp.BetModified)
                                                        from `antifraud-testing.dwh_kafka_sink_1.connect_dwh_sportsbook_betselection_v3`  as tmp
                                                        where tmp.BetId = bs.BetId)
                                group by
                                      bs.BetId,
                                      bs.ClientId,
                                      bs.BetType,
                                      bs.BetState,
                                      bs.PartnerId,
                                      bs.BetCreated,
                                      bs.betAmountInEURC,
                                      bs.BetWinningAmountInEURC,
                                      bs.BetCurrencyId,
                                      bs.ClientBirthRegionId,
                                      bs.BetSource,
                                      bs.BetIsLive,
                                      bs.SportId
                                      ) as final
where final.row_number = 1