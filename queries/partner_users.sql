with valid_data as 
(
select
  *
from 
  `antifraud-testing.dwh_kafka_sink_1.connect_dwh_sportsbook_betselection_v3` as bs
where
  bs.BetCreated >= '{}' and
  bs.BetCreated <= '{}' and
  bs.PartnerId = {} and
  bs.BetCalcDate is not null and
  bs.BetModified = (select max(tmp.BetModified) 
                  from `antifraud-testing.dwh_kafka_sink_1.connect_dwh_sportsbook_betselection_v3` as tmp
                  where tmp.BetId = bs.BetId)),

all_clients as (
  select 
        bs.ClientId as ClientId,
        bs.PartnerId
  from 
        valid_data as bs 
  group by
        bs.ClientId,
        bs.PartnerId
),

client_bets as (
  select
        bs.ClientId as ClientId,
        bs.BetId  as BetId,
        bs.BetIsLive as IsLive,
        bs.PartnerId as PartnerId,
        bs.BetType as Type
  from 
        valid_data  as bs
  group by
        bs.ClientId,
        bs.BetId,
        bs.BetIsLive,
        bs.PartnerId,
        bs.BetType
),

clients_line_type as (
select 
  bs.ClientId,
  bs.PartnerId,
  ROUND(COUNTIF(bs.IsLive = 1) / count(*) * 100, 2) as live_ratio, 
  ROUND(COUNTIF(bs.IsLive = 0) / count(*) * 100, 2) as prematch_ratio
from
  client_bets as bs
group by
  bs.ClientId,
  bs.PartnerId
),

clients_bet_type as (
select 
  bs.ClientId,
  bs.PartnerId,
  ROUND(COUNTIF(bs.Type = 1) / count(*) * 100, 2) as single_ratio, 
  ROUND(COUNTIF(bs.Type != 1) / count(*) * 100, 2) as express_ratio
from
  client_bets as bs
group by
  bs.ClientId,
  bs.PartnerId
),

player_reg_diff as (
select
  bs.ClientId as ClientId,
  bs.PartnerId as PartnerId,
  case when DATETIME_DIFF(EXTRACT(DATETIME from MIN(bs.BetCreated)),
                EXTRACT(DATETIME from bs.ClientCreated),
                MONTH) > 1 then 0 else 1 end as new_user,
  case when DATETIME_DIFF(EXTRACT(DATETIME from MIN(bs.BetCreated)),
                EXTRACT(DATETIME from bs.ClientCreated),
                MONTH) > 1 then 1 else 0 end as old_user
from
  valid_data as bs
group by
  bs.ClientId,
  bs.ClientCreated,
  bs.PartnerId
),

bet_sport_counts as (
  select
        bs.ClientId as ClientId,
        bs.BetId  as BetId,
        bs.PartnerId as PartnerId,
        bs.SportId as SportId,
        COUNT(bs.SportId) as SportIdCount
  from 
        valid_data  as bs
  group by
        bs.ClientId,
        bs.BetId,
        bs.PartnerId,
        bs.SportId

),

bet_sport as (
select 
  bs.ClientId as ClientId,
  bs.BetId  as BetId,
  bs.PartnerId as PartnerId,
  bs.SportId as SportId
from 
  bet_sport_counts as bs
where 
  bs.SportIdCount = (select MAX(tmp.SportIdCount) from bet_sport_counts as tmp where bs.ClientId=tmp.ClientId and bs.BetId=tmp.BetId and bs.PartnerId=tmp.PartnerId)
),

clients_sports as (
select 
  bs.ClientId,
  bs.PartnerId,
  ROUND(COUNTIF(bs.SportId = 1) / count(*) * 100, 2) as football_ratio,
  ROUND(COUNTIF(bs.SportId = 57) / count(*) * 100, 2) as virtual_football_ratio, 
  ROUND(COUNTIF(bs.SportId = 71) / count(*) * 100, 2) as cyber_football_ratio, 
  
  ROUND(COUNTIF(bs.SportId = 3) / count(*) * 100, 2) as basketball_ratio, 
  ROUND(COUNTIF(bs.SportId = 4) / count(*) * 100, 2) as tennis_ratio, 
  ROUND(COUNTIF(bs.SportId = 41) / count(*) * 100, 2) as table_tennis_ratio, 
  ROUND(COUNTIF(bs.SportId = 5) / count(*) * 100, 2) as volleyball_ratio, 
  ROUND(COUNTIF(bs.SportId = 2) / count(*) * 100, 2) as ice_hockey_ratio, 
  ROUND(COUNTIF(bs.SportId = 174) / count(*) * 100, 2) as penalties_ratio,
  
  ROUND(COUNTIF(bs.SportId = 47) / count(*) * 100, 2) as VirtualSports_ratio,
  ROUND(COUNTIF(bs.SportId = 56) / count(*) * 100, 2) as VirtualTennis_ratio,
  ROUND(COUNTIF(bs.SportId = 73) / count(*) * 100, 2) as EBasketball_ratio,
  ROUND(COUNTIF(bs.SportId = 139) / count(*) * 100, 2) as ETennis_ratio,
  ROUND(COUNTIF(bs.SportId = 175) / count(*) * 100, 2) as EIceHockey_ratio,
  
  ROUND(COUNTIF(bs.SportId in (75, 76, 77, 164, 176)) / count(*) * 100, 2) as CyberSport_ratio,
  ROUND(COUNTIF(bs.SportId not in (1, 57, 71, 3, 4, 41, 5, 2, 174, 47, 56, 73, 139, 175)) / count(*) * 100, 2) as other_ratio
from
  bet_sport as bs
group by
  bs.ClientId,
  bs.PartnerId
)

select
  c.ClientId as ClientId,
  p_r.new_user as NewUser,
  p_r.old_user as OldUser,
  bt.single_ratio as Single,
  bt.express_ratio as Express, 
  bs.live_ratio as Live,
  bs.prematch_ratio as Prematch,
  football_ratio as Soccer,
  virtual_football_ratio as VirtualFootball, 
  cyber_football_ratio as CyberFootball, 
  basketball_ratio as Basketball, 
  tennis_ratio as Tennis, 
  table_tennis_ratio as TableTennis, 
  volleyball_ratio as Volleyball, 
  ice_hockey_ratio as IceHockey, 
  penalties_ratio as PenaltyKicks,
  VirtualSports_ratio as VirtualSports,
  VirtualTennis_ratio as VirtualTennis,
  EBasketball_ratio as EBasketball,
  ETennis_ratio as ETennis,
  EIceHockey_ratio as EIceHockey,
  CyberSport_ratio as CyberSport,
  other_ratio as OtherSports
from 
  all_clients as c join
  player_reg_diff as p_r on p_r.ClientId = c.ClientId and p_r.PartnerId = c.PartnerId join
  clients_bet_type as bt on bt.ClientId = c.ClientId and bt.PartnerId = c.PartnerId join
  clients_line_type as bs on bs.ClientId = c.ClientId and bs.PartnerId = c.PartnerId join
  clients_sports as cs on cs.ClientId = c.ClientId and cs.PartnerId = c.PartnerId
