#!/bin/bash
ARG1=${1:-8888}
docker run \
--user $(id -u):$(id -g) \
-p $ARG1:$ARG1 \
--mount type=bind,source="$PWD/../",target=/project -it --rm \
$(whoami)/sportsbook:env bash