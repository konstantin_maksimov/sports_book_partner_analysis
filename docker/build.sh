#!/usr/bin/env bash
cp ../requirements.txt ./requirements.txt
docker build -t $(whoami)/sportsbook:env \
--build-arg user_name=$(whoami) --build-arg user_id=$(id -u) .
rm requirements.txt