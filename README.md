# SporsBook partner analysis

After project clone start docker with default port 8888

```
cd docker
source build.sh
source run.sh
```
To start docker with custom port
```
cd docker
source build.sh
source run.sh 9999
```

To start jupyter inside docker
```
source start_jupyter.sh
```

To start jupyter with same custom port as docker start
```
source start_jupyter.sh 9999
```